#![crate_name = "sudoku_solver"]

mod structs;
mod import_util;
#[macro_use]
extern crate log;
extern crate simple_logger;
#[macro_use]
extern crate clap;
use import_util::import_csv_from_path;
use log::Level;
use clap::{Arg, App};
use std::env;
use std::fs;

fn main() {
    //simple_logger::init().unwrap();
    simple_logger::init_with_level(Level::Info).unwrap();

    let matches = App::new(crate_name!())
        .about(crate_description!())
        .author(crate_authors!())
        // use crate_version! to pull the version number
        .version(crate_version!())
        .arg(
            Arg::with_name("file")
                .short("f")
                .long("file")
                .takes_value(true)
                .default_value("./resources/example_puzzle.csv")
                .help("path to file e.g. ./resources/example_puzzle.csv")
                .required(true),
        )
        .get_matches();

    if let Some(file) = matches.value_of("file") {
        println!("A puzzle file was passed in: {}", file);

        let contents = fs::read_to_string(file)
            .expect("Something went wrong reading the file");

        println!("With text:\n{}", contents);

        println!("====================");
        println!("From csv");
        let mut board = import_csv_from_path(file);
        info!("Imported board from file:");
        board.print_all_cell_info();
        board.solve();
        info!("Solved:");
        board.print_all_cell_info();
    }
}