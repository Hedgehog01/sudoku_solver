use std::time::{Instant};
use array2d::Array2D;
use serde::Deserialize;

/// Represents a single cell inside a Sudoku board
///
#[derive(Debug, Copy, Clone, PartialEq, Eq, Deserialize)]
pub enum Cell {
    Empty = 0,
    One = 1,
    Two = 2,
    Three = 3,
    Four = 4,
    Five = 5,
    Six = 6,
    Seven = 7,
    Eight = 8,
    Nine = 9,
}

impl Cell {
    /// Return a number representation to each enum value
    pub fn get_number(self) -> usize {
        match self {
            Self::Empty => 0,
            Self::One => 1,
            Self::Two => 2,
            Self::Three => 3,
            Self::Four => 4,
            Self::Five => 5,
            Self::Six => 6,
            Self::Seven => 7,
            Self::Eight => 8,
            Self::Nine => 9
        }
    }
    /// Return a Cell from a given number between 0-9
    ///
    /// # Arguments
    ///
    /// * `num` - a number between 0-9 that represent a Sudoku cell
    ///
    /// # Example
    ///
    /// ```
    /// use sudoku_solver::Cell;
    /// let cell = Cell::value_from_number(2);
    /// ```
    ///
    /// # Panics
    /// Will panic if input value is <> 0-9
    pub fn value_from_number(num: usize) -> Cell {
        match num {
            0 => Self::Empty,
            1 => Self::One,
            2 => Self::Two,
            3 => Self::Three,
            4 => Self::Four,
            5 => Self::Five,
            6 => Self::Six,
            7 => Self::Seven,
            8 => Self::Eight,
            9 => Self::Nine,
            _ => panic!("Non existent Cell value entered")
        }
    }

    ///
    /// Returns true if the cell is empty or false if has other value.
    /// Empty cell is represented by Cell::Empty, i.e cell numeric value is 0
    /// # Examples
    /// ```
    ///     let cell = Cell::Empty;
    ///     assert_eq!(true, cell.isEmpty());
    /// ```
    ///
    fn is_empty(self) -> bool {
        if self.get_number() == Cell::Empty.get_number() {
            return true;
        }
        false
    }
}


/// A Sudoku board is represented here
#[derive(Debug, Eq)]
pub struct Board {
    /// A Sudoku being is represented here
    width: usize,
    height: usize,
    cells: Array2D<Cell>,
    //an array to hold the nine nonets
    nonets: Vec<Array2D<Cell>>,
    possible_solution: Array2D<Vec<Cell>>,
    number_of_empty_cells_on_board: usize,
}


impl Board {
    /// Returns an new Board
    ///
    ///
    /// # Example
    ///
    /// ```
    /// // You can have rust code between fences inside the comments
    /// // If you pass --test to Rustdoc, it will even test it for you!
    /// use doc::Board;
    /// let board = Board::new();
    /// ```
    pub fn new() -> Board {
        let width: usize = 9;
        let height: usize = 9;

        let cells = Array2D::filled_with(Cell::Empty, width, height);
        let nonets = vec![Array2D::filled_with(Cell::Empty, 3, 3); 9];
        let possible_solution = Array2D::filled_with(Vec::new(), width, height);
        let number_of_empty_cells_on_board = 81;
        Board {
            width,
            height,
            cells,
            nonets,
            possible_solution,
            number_of_empty_cells_on_board,
        }
    }

    // fill board fields from inputted cells
    pub fn update_board_from_cells(&mut self, cells: Array2D<Cell>) {
        self.cells = cells;
        self.fill_nonets();
        self.set_number_of_empty_cells_on_board();
    }

    //Sets the field number_of_empty_cells_on_board to the number of empty cells
    fn set_number_of_empty_cells_on_board(&mut self) {
        let mut count = 0;
        for (_row, row_iter) in self.cells.rows_iter().enumerate() {
            for (_col, element) in row_iter.enumerate() {
                if element.is_empty() {
                    count += 1;
                }
            }
        }
        self.number_of_empty_cells_on_board = count;
    }

    fn fill_nonets(&mut self) {
        for i in 0..9 {
            self.fill_nonet(i);
        }
    }

    ///get the nine nonets start x and y pos
    fn get_nonet_start_x_y(nonet_num: usize) -> (usize, usize) {
        match nonet_num {
            0 => (0, 6),
            1 => (3, 6),
            2 => (6, 6),
            3 => (0, 3),
            4 => (3, 3),
            5 => (6, 3),
            6 => (0, 0),
            7 => (3, 0),
            8 => (6, 0),
            _ => panic!("Invalid nonet number entered")
        }
    }

    fn get_nonet_num_by_x_y(input_x: usize, input_y: usize) -> usize {
        for i in 0..9 {
            let (x, y) = Board::get_nonet_start_x_y(i);
            //check if is in same x & y range as x & y of a nonet to find the correct one
            if (input_x >= x && (input_x - x) < 3) && (input_y >= y && (input_y - y) < 3) {
                trace!("{}", format!("X:{} Y:{} in nonet: {}", input_x, input_y, i));
                return i;
            }
        }
        panic!(format!("Could not detect what nonet x:{} y:{} is in", input_x, input_y))
    }

    fn fill_nonet(&mut self, nonet_num: usize) {
        let nonet_start_point_x_y = Board::get_nonet_start_x_y(nonet_num);
        let mut nonets_to_fill = Array2D::filled_with(Cell::Empty, 3, 3);
        let mut x = 0;
        for i in nonet_start_point_x_y.0..nonet_start_point_x_y.0 + 3 {
            let mut y = 0;
            for j in nonet_start_point_x_y.1..nonet_start_point_x_y.1 + 3 {

                //get cell to put in the nonet
                let cell = self.cells.get(i as usize, j as usize).expect("Should have Cell value");
                if cell.get_number() != Cell::Empty.get_number() {
                    // trace!("{}", format!("adding cell {} to nonet: {}, x: {}, y: {}", cell.get_number(), nonet_num, i, j));
                    nonets_to_fill.set(x, y, *cell).expect("Could not set cell!");
                }
                y += 1;
            }
            x += 1;
        }
        //put the nonet in the nonet vec
        self.nonets.insert(nonet_num, nonets_to_fill);
    }

    pub fn solve(&mut self) {
        if self.number_of_empty_cells_on_board == 81 {
            panic!("Board values do not appear to have been set! please check input and try again.")
        }

        let now = Instant::now();
        // for i in 0..99 {
        //     trace!("attempt to fill: {}", i);
        self.fill_possible_cell_options();
        let mut i = 0;
        while !self.is_number_of_empty_cells_on_board_zero() {
            i += 1;
            trace!("Running. Iteration number {} ...", i);
            self.fill_cells();
            self.reset_single_solutions_array();
            self.fill_possible_cell_options();
            self.fill_nonets();
            //if inputted puzzle is incorrect prevent endless loop
            if i >= 81 {
                panic!("Something went wrong. Could not solve the puzzle. Please check the inputted data!")
            }
        }
        self.fill_nonets();
        info!("Puzzle Solved!");
        info!("Time to solve (Milliseconds): {}", now.elapsed().as_millis());
    }

    pub fn print_all_cell_info(&self) {
        println!("Printing full board");
        for (row, row_iter) in self.cells.rows_iter().enumerate() {
            for (col, _element) in row_iter.enumerate() {
                let x = self.cells.get(8 - row, col).unwrap().get_number();
                print!("{}", format!("|{}|", x));
            }
            println!();
        }
    }

    fn reset_single_solutions_array(&mut self) {
        for x in 0..9 {
            for y in 0..9 {
                trace!("reseting vec");
                self.possible_solution.set(x, y, vec![]).expect("Could not set new vec to possible solution array!");
            }
        }
    }

    // check if the value of number_of_empty_cells_on_board is 0
    fn is_number_of_empty_cells_on_board_zero(&self) -> bool {
        self.number_of_empty_cells_on_board == 0
    }

    fn fill_possible_cell_options(&mut self) {
        for row in 0..self.width {
            for col in 0..self.height {
                let cell = &self.cells.get(row, col).expect("Expected to get Cell");
                if cell.is_empty() {
                    self.fill_possibles_for_cell(row, col);
                }
            }
        }
    }

    fn fill_possibles_for_cell(&mut self, row: usize, col: usize) {
        for i in 1..10 {
            let possible_cell = Cell::value_from_number(i);
            if self.can_be_placed_in_cell(row, col, possible_cell) {
                trace!("{}", format!("Adding possible solution: {}, to row: {}, column: {}", possible_cell.get_number(), row, col));
                let mut x: std::vec::Vec<Cell> = self.possible_solution.get(row, col).unwrap().to_vec();
                x.push(possible_cell);
                self.possible_solution.set(row, col, x).expect("Could not set cell!");
            }
        }
    }

    pub fn fill_cells(&mut self) {
        for row in 0..self.width {
            for col in 0..self.height {
                if self.cells.get(row, col).unwrap().is_empty() {
                    let cell = self.get_available(row, col);
                    if cell.get_number() != 0 {
                        self.number_of_empty_cells_on_board -= 1;
                    }

                    println!("number of empty cells on board: {}", self.number_of_empty_cells_on_board);
                    self.cells.set(row, col, cell).expect("Could not set cell!");
                }
            }
        }
    }

    pub fn get_available(&self, row: usize, column: usize) -> Cell {
        //check if cell already has value
        if self.cells.get(row, column).expect("Expected to find Cell").get_number() != Cell::Empty.get_number() {
            return *self.cells.get(row, column).expect("Expected to find Cell");
        }

        if self.possible_solution.get(row, column).unwrap().len() == 1 {
            let possible = *self.possible_solution.get(row, column).unwrap().get(0).unwrap();
            trace!("{}", format!("Getting the only solution for row: {} col: {} possible solution: {}", row, column, possible.get_number()));
            return possible;
        }
        Cell::Empty
    }

    fn is_available_in_nonet(&self, row: usize, col: usize, cell: Cell) -> bool {
        //get nonet for the row and column so can confirm num is not in the nonet
        let nonet_num = Board::get_nonet_num_by_x_y(row, col);
        let nonet_to_check = self.nonets.get(nonet_num).unwrap();
        trace!("{}", format!("x: {} y: {} nonet num: {}", row, col, nonet_num));
        // Iterate over all rows or columns.
        for row_iter in nonet_to_check.rows_iter() {
            for element in row_iter {
                if element.get_number() == cell.get_number() {
                    return false;
                }
            }
        }
        if nonet_num == 2 && cell.get_number() == 5 {
            trace!("nonet 2 cell 5. Nonet: {:?}", self.nonets.get(2));
        }
        true
    }

    fn can_be_placed_in_cell(&self, row: usize, col: usize, cell: Cell) -> bool {
        //if cell is not empty can't place other value there
        if !self.cells.get(row, col).unwrap().is_empty() {
            trace!("Cell {} not empty", cell.get_number());
            return false;
        }
        //verify num doesn't appear on same row or column as the num
        if Board::is_available_on_row(&self, row, cell) && Board::is_available_on_column(&self, col, cell) && self.is_available_in_nonet(row, col, cell) {
                trace!("{}", format!("can be placed: Cell: {} at row: {}, col: {}", cell.get_number(), row, col));
                return true;
        }
        trace!("{}", format!("can't be placed: Cell: {} at row: {}, col: {}", cell.get_number(), row, col));
        false
    }

    fn is_available_on_row(&self, row: usize, cell: Cell) -> bool {
        for col in 0..self.width as usize {
            if self.cells.get(row, col).unwrap().get_number() == cell.get_number() {
                trace!("Cell: {} was found on row: {} can't be added", cell as usize, row);
                return false;
            }
        }
        true
    }

    fn is_available_on_column(&self, col: usize, cell: Cell) -> bool {
        for row in 0..self.height as usize {
            if self.cells.get(row, col).unwrap().get_number() == cell.get_number() {
                trace!("Cell: {} was found on column: {} can't be added", cell as usize, col);
                return false;
            }
        }
        true
    }
}

impl PartialEq for Board {
    fn eq(&self, other: &Self) -> bool {
        self.cells.eq(&other.cells)
    }
}

#[cfg(test)]
mod board_tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;

    #[test]
    fn update_board_from_cells(){
        let mut board_under_test = Board::new();
        assert_eq!(board_under_test.number_of_empty_cells_on_board,81);
        let array_cells_to_add = get_unsolved_rows();
        board_under_test.update_board_from_cells(array_cells_to_add);
        assert_eq!(board_under_test.number_of_empty_cells_on_board,47);
    }

    // Tests that solve function actually solves correctly
    #[test]
    fn solve() {
        let mut attempted_solved_board = initialize_unsolved_board();
        let expected_solved_board = initialize_solved_board();
        attempted_solved_board.solve();
        println!("Solved:");
        attempted_solved_board.print_all_cell_info();
        println!("Expected");
        expected_solved_board.print_all_cell_info();
        assert_eq!(attempted_solved_board == expected_solved_board, true);
    }


    fn initialize_solved_board() -> Board {
        get_solved()
    }

    fn initialize_unsolved_board() -> Board {
        let mut board = Board::new();
        board.cells = get_unsolved_rows();
        board.number_of_empty_cells_on_board = 47;
        board.fill_nonets();
        board
    }

    fn get_solved() -> Board{
        let mut board = Board::new();
        let rows = vec![
            //row 0
            vec![Cell::Four, Cell::Two, Cell::Nine, Cell::Eight, Cell::One, Cell::Six, Cell::Three, Cell::Five, Cell::Seven],
            //row 1
            vec![Cell::One, Cell::Six, Cell::Seven, Cell::Five, Cell::Three, Cell::Two, Cell::Four, Cell::Nine, Cell::Eight],
            //row 2
            vec![Cell::Three, Cell::Five, Cell::Eight, Cell::Nine, Cell::Seven, Cell::Four, Cell::Six, Cell::Two, Cell::One],
            //row 3
            vec![Cell::Seven, Cell::Four, Cell::Two, Cell::One, Cell::Nine, Cell::Five, Cell::Eight, Cell::Six, Cell::Three],
            //row 4
            vec![Cell::Six, Cell::One, Cell::Three, Cell::Seven, Cell::Two, Cell::Eight, Cell::Nine, Cell::Four, Cell::Five],
            //row 5
            vec![Cell::Nine, Cell::Eight, Cell::Five, Cell::Six, Cell::Four, Cell::Three, Cell::Seven, Cell::One, Cell::Two],
            //row 6
            vec![Cell::Eight, Cell::Three, Cell::Six, Cell::Four, Cell::Five, Cell::One, Cell::Two, Cell::Seven, Cell::Nine],
            //row 7
            vec![Cell::Two, Cell::Seven, Cell::One, Cell::Three, Cell::Six, Cell::Nine, Cell::Five, Cell::Eight, Cell::Four],
            //row 8
            vec![Cell::Five, Cell::Nine, Cell::Four, Cell::Two, Cell::Eight, Cell::Seven, Cell::One, Cell::Three, Cell::Six]
        ];
        board.cells = Array2D::from_rows(&rows);
        board.fill_nonets();
        board.number_of_empty_cells_on_board = 0;
        board
    }

    fn get_unsolved_rows() -> Array2D<Cell> {
        let rows = vec![
            //row 0
            vec![Cell::Four, Cell::Empty, Cell::Empty, Cell::Empty, Cell::One, Cell::Empty, Cell::Empty, Cell::Empty, Cell::Empty],
            //row 1
            vec![Cell::One, Cell::Six, Cell::Empty, Cell::Five, Cell::Empty, Cell::Two, Cell::Empty, Cell::Nine, Cell::Empty],
            //row 2
            vec![Cell::Empty, Cell::Five, Cell::Eight, Cell::Nine, Cell::Empty, Cell::Four, Cell::Six, Cell::Empty, Cell::Empty],
            //row 3
            vec![Cell::Seven, Cell::Four, Cell::Empty, Cell::Empty, Cell::Empty, Cell::Empty, Cell::Empty, Cell::Six, Cell::Empty],
            //row 4
            vec![Cell::Empty, Cell::Empty, Cell::Three, Cell::Seven, Cell::Empty, Cell::Eight, Cell::Nine, Cell::Empty, Cell::Empty],
            //row 5
            vec![Cell::Empty, Cell::Eight, Cell::Empty, Cell::Empty, Cell::Empty, Cell::Empty, Cell::Empty, Cell::One, Cell::Two],
            //row 6
            vec![Cell::Empty, Cell::Empty, Cell::Six, Cell::Four, Cell::Empty, Cell::One, Cell::Two, Cell::Seven, Cell::Empty],
            //row 7
            vec![Cell::Empty, Cell::Seven, Cell::Empty, Cell::Three, Cell::Empty, Cell::Nine, Cell::Empty, Cell::Eight, Cell::Four],
            //row 8
            vec![Cell::Empty, Cell::Empty, Cell::Empty, Cell::Empty, Cell::Eight, Cell::Empty, Cell::Empty, Cell::Empty, Cell::Six]
        ];
        Array2D::from_rows(&rows)
    }
}
