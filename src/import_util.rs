use csv::{ReaderBuilder};
use std::str::FromStr;
use array2d::Array2D;
use crate::structs::{Cell, Board};

pub fn import_csv_from_path(filepath: &str) -> Board {
    let cells = get_array2d_from_csv(filepath);
    let mut board = Board::new();
    board.update_board_from_cells(cells);
    board
}

fn get_array2d_from_csv(file_path: &str) -> Array2D<Cell> {
    let mut rdr = ReaderBuilder::new()
        .has_headers(false).from_path(&file_path).unwrap_or_else(|_| panic!("Could not open file!: {}", file_path));
    let mut cells = Array2D::filled_with(Cell::Empty, 9, 9);
    for (row, result) in rdr.records().enumerate() {
        let record = result.expect("Expected String record");
        for (col, rcrd) in record.iter().enumerate() {
            let value = usize::from_str(rcrd).expect("Should have been number");
            cells.set(8 - row, col, Cell::value_from_number(value)).expect("Could not sent cell");
            println!("{}", format!("Row: {}, Col: {} , value: {}", 8 - row, col, value));
        }
    }
    cells
}

#[cfg(test)]
mod import_util_tests {
    // importing names from outer (for mod tests) scope.
    use super::*;

    #[test]
    fn test_import_from_csv_valid_file() {
        let actual_result = super::get_array2d_from_csv("./resources/example_puzzle.csv");
        let expected_result = get_expected_rows();
        assert_eq!(actual_result, expected_result)
    }

    #[test]
    #[should_panic(expected = "Could not open file!: ./resources/example_puzzle100.csv")]
    fn test_import_from_csv_invalid_file() {
        let actual_result = super::get_array2d_from_csv("./resources/example_puzzle100.csv");
        let expected_result = get_expected_rows();
        assert_eq!(actual_result, expected_result)
    }

    fn get_expected_rows() -> Array2D<Cell> {
        let rows = vec![
            //row 0
            vec![Cell::Four, Cell::Empty, Cell::Empty, Cell::Empty, Cell::One, Cell::Empty, Cell::Empty, Cell::Empty, Cell::Empty],
            //row 1
            vec![Cell::One, Cell::Six, Cell::Empty, Cell::Five, Cell::Empty, Cell::Two, Cell::Empty, Cell::Nine, Cell::Empty],
            //row 2
            vec![Cell::Empty, Cell::Five, Cell::Eight, Cell::Nine, Cell::Empty, Cell::Four, Cell::Six, Cell::Empty, Cell::Empty],
            //row 3
            vec![Cell::Seven, Cell::Four, Cell::Empty, Cell::Empty, Cell::Empty, Cell::Empty, Cell::Empty, Cell::Six, Cell::Empty],
            //row 4
            vec![Cell::Empty, Cell::Empty, Cell::Three, Cell::Seven, Cell::Empty, Cell::Eight, Cell::Nine, Cell::Empty, Cell::Empty],
            //row 5
            vec![Cell::Empty, Cell::Eight, Cell::Empty, Cell::Empty, Cell::Empty, Cell::Empty, Cell::Empty, Cell::One, Cell::Two],
            //row 6
            vec![Cell::Empty, Cell::Empty, Cell::Six, Cell::Four, Cell::Empty, Cell::One, Cell::Two, Cell::Seven, Cell::Empty],
            //row 7
            vec![Cell::Empty, Cell::Seven, Cell::Empty, Cell::Three, Cell::Empty, Cell::Nine, Cell::Empty, Cell::Eight, Cell::Four],
            //row 8
            vec![Cell::Empty, Cell::Empty, Cell::Empty, Cell::Empty, Cell::Eight, Cell::Empty, Cell::Empty, Cell::Empty, Cell::Six]
        ];
        Array2D::from_rows(&rows)
    }
}